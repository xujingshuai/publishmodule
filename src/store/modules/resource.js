/**
 * @Author:      skyeGao
 * @Email:       yyjzp1314@126.com
 * @DateTime:    2019-04-06 12:03:12
 * @Description: 公共 actions 
 */

import {
    getEbookContent,
    getRelationResource,
    articalNoteAdd,
    articalNoteUpdate,
    articalNoteDel,
    articalNoteList,
    getEbookCatalogue,
    saveCorrect
  } from '@/api/resource'
  import { resolve } from 'upath'
  
const resource = {
    state: { },
  
    mutations: { },
  
    actions: {
      // 阅读器 - 获取电子书内容
      GetEBookContent({ commit }, params) {
        return new Promise((resolve, reject) => {
          getEbookContent(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 阅读器 - 获取电子书目录
      GetBookInfo({ commit }, params) {
        return new Promise((resolve, reject) => {
          getEbookCatalogue(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 阅读器 - 保存纠错
      ArticalErrorCorrect({ commit }, params) {
        return new Promise((resolve, reject) => {
          saveCorrect(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 文章阅读页 - 笔记新增
      ArticalNoteAdd({ commit }, params) {
        return new Promise((resolve, reject) => {
          articalNoteAdd(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
       // 文章阅读页 - 笔记修改
      ArticalNoteUpdate({ commit }, params) {
        return new Promise((resolve, reject) => {
          articalNoteUpdate(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 文章阅读页 - 笔记删除
      ArticalNoteDel({ commit }, params) {
        return new Promise((resolve, reject) => {
          articalNoteDel(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 文章阅读页 - 笔记列表
      ArticalNoteList({ commit }, params) {
        return new Promise((resolve, reject) => {
          articalNoteList(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 电子书阅读页 - 右侧关联资源
      EbookRelationRes({ commit }, params) {
        return new Promise((resolve, reject) => {
          getRelationResource(params).then(response => {
            const data = response.data;
            resolve(data)
          }).catch(error => {
            reject(error)
          })
        })
      },
    }
}
  
export default resource
  