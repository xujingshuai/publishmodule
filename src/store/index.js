import Vue from 'vue'
import Vuex from 'vuex'
import Cookies from 'js-cookie';
import { messageAPI } from '@/api/common'
import resource from './modules/resource'

Vue.use(Vuex)
const state = {
    searchContent: null,
    userPhone: null,
    messageNum: null,
}
const store = new Vuex.Store({
    modules: {
        resource
    },
    mutations: {
        changeSearchWord(state, n) {
            state.searchContent = n.searchContent;
        },
        setPhone(state, n) {
            state.userPhone = n;
        },
        SET_MESSAGE_NUM(state) {
            messageAPI().then(res => {
                if (res.data.data > 0) {
                    state.messageNum = res.data.data
                    // console.log(state.messageNum,999999);
                } else {
                    state.messageNum = null
                }
            })
        },
    },
    actions: {
        setMessageNum({ commit }, view) {
            commit('SET_MESSAGE_NUM', view)
        },
    }
})





export default store