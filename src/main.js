// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import '@static/css/common.less'
import App from './App'
import Cookies from 'js-cookie';
import { uploadUrl } from '@/utils/global'
import loginAlert from '@/components/loginAlert.vue';
import store from './store';
import Viewer from 'v-viewer'
import VDistpicker from 'v-distpicker'

Vue.use(ElementUI)

Vue.use(Viewer, {
    defaultOptions: {
        zIndex: 9999
    }
});

Vue.component('loginAlert', loginAlert)

Vue.component('v-distpicker', VDistpicker)

Vue.config.productionTip = false
    /* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})

router.beforeEach(({ meta, path }, from, next) => {
    next();
    window.scrollTo(0, 0)
})

/*图片路径*/
Vue.prototype.fileUrl = function(link) { //"http://192.168.2.231:8696/file/file/?fileName=3737b4397b9e4ec7ab3538c5b11ee6ea
        return `${uploadUrl}file/?fileName=` + link
    }
    /*音频路径*/
    // Vue.prototype.audioUrl = function (link) {//"http://192.168.2.231:8696/file/file/?fileName=3737b4397b9e4ec7ab3538c5b11ee6ea
    //   return `http://192.168.2.231/` + link
    // }
Vue.filter('limit', function(value) {
    if (value != undefined) {
        var value = value.replace(/<\/?[^>]*>/g, ''); //去除HTML Tag
        value = value.replace(/[|]*\n/, '') //去除行尾空格
        value = value.replace(/&nbsp;/ig, ''); //去掉npsp
    }
    var val = value;
    if (value && value.length > 90) {
        var val = (value.replace(/\s+/g, "")).substr(0, 90) + " ..."
    }
    return val
})