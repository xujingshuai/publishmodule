import Vue from 'vue'
import Router from 'vue-router'
import perHeader from '@/components/perHeader.vue'
import LoginHeader from '@/components/LoginHeader.vue'
import comheader from '@/components/comHeader'
import specialHeader from '@/components/specialHeader'

Vue.use(Router)

export const constantRouterMap = [{
        path: '/',
        name: 'index',
        meta: { title: '主页面' },
        component: () =>
            import ('@/views/home/index')
    },
    {
        path: '/index',
        name: 'index',
        meta: { title: '主页面' },
        component: () =>
            import ('@/views/home/index')
    },
    {
        path: '/bookhome',
        name: 'bookhome',
        meta: '图书首页',
        component: () =>
            import ('@/views/home/book_home'),
    },
    //专题--课程
    {
        path: '/commonheader',
        name: 'commonheader',
        component: comheader,
        children: [{
            path: 'subject',
            name: 'subject',
            meta: { title: '专题列表' },
            component: () =>
                import ('@/views/home/subject'),
        }, {
            path: 'subjectDetails',
            name: 'subjectDetails',
            meta: { title: '专题详情' },
            component: () =>
                import ('@/views/home/subject-details'),
        }, {
            path: 'coursehome',
            name: 'coursehome',
            meta: { title: '在线课程' },
            component: () =>
                import ('@/views/home/course-home'),
        }, {
            path: 'courseDetails',
            name: 'courseDetails',
            meta: { title: '在线课程详情', fatherName: 'coursehome' },
            component: () =>
                import ('@/views/home/course-details'),
        }, {
            path: 'coursepage',
            name: 'coursepage',
            meta: { title: '课程推荐' },
            component: () =>
                import ('@/views/home/course'),
        }, {
            path: 'sortpage',
            name: 'sortpage',
            meta: { title: '学科分类' },
            component: () =>
                import ('@/views/home/sortpage'),
        }]
    },
    // 资源
    {
        path: '/resource',
        name: 'resource',
        component: comheader,
        // meta: { title: '全文检索' },
        redirect: '/resource/commonleft/resource_book',
        children: [{
            path: 'commonleft',
            name: 'commonleft',
            component: () =>
                import ('@/views/resource/common_left'),
            children: [{
                path: 'resource_book',
                name: 'resource_book',
                meta: { title: '图书' },
                component: () =>
                    import ('@/views/resource/book'),
            }, {
                path: 'resource_article',
                name: 'resource_article',
                meta: { title: '章节' },
                component: () =>
                    import ('@/views/resource/article'),
            }, {
                path: 'resource_image',
                name: 'resource_image',
                meta: { title: '图片' },
                component: () =>
                    import ('@/views/resource/image'),
            }, {
                path: 'resource_video',
                name: 'resource_video',
                meta: { title: '视频' },
                component: () =>
                    import ('@/views/resource/video'),
            }, , {
                path: 'resource_audio',
                name: 'resource_audio',
                meta: { title: '音频' },
                component: () =>
                    import ('@/views/resource/audio'),
            }, {
                path: 'resource_file',
                name: 'resource_file',
                meta: { title: '课件' },
                component: () =>
                    import ('@/views/resource/file'),
            }, {
                path: 'resource_doctor',
                name: 'resource_doctor',
                meta: { title: '学者' },
                component: () =>
                    import ('@/views/resource/doctor'),
            }, ]
        }, {
            path: 'bookDetails',
            name: 'bookDetails',
            meta: 　{ title: '全部', fatherName: 'resource_book' },
            component: () =>
                import ('@/views/resource/book_details')
        }, {
            path: 'articleread',
            name: 'articleread',
            meta: { title: '章节详情', fatherName: 'resource_article' },
            component: () =>
                import ('@/views/resource/article_details')

        }, {
            path: 'imageDetails',
            name: 'imageDetails',
            meta: { title: '图片详情', fatherName: 'resource_image' },
            component: () =>
                import ('@/views/resource/image_details')

        }, {
            path: 'videoDetails',
            name: 'videoDetails',
            meta: { title: '视频详情', fatherName: 'resource_video' },
            component: () =>
                import ('@/views/resource/video_details')

        }, {
            path: 'fileDetails',
            name: 'fileDetails',
            meta: { title: '课件详情', fatherName: 'resource_file' },
            component: () =>
                import ('@/views/resource/file_details')

        }, {
            path: 'audioDetails',
            name: 'audioDetails',
            meta: { title: '音频详情', fatherName: 'resource_audio' },
            component: () =>
                import ('@/views/resource/audio_details')

        }, {
            path: 'doctorDetails',
            name: 'doctorDetails',
            meta: { title: '学者详情', fatherName: 'resource_doctor' },
            component: () =>
                import ('@/views/resource/doctor_details')
        }],
    },
    //资讯
    {
        path: '/medical',
        name: 'medical',
        component: comheader,
        redirect: '/resource/commonleft/resource_book',
        meta: { title: '资讯' },
        redirect: '/medical/medicine',
        children: [{
            path: 'medicine',
            name: 'medicine',
            meta: { title: '资讯列表' },
            component: () =>
                import ('@/views/information/medicine')
        }, {
            path: 'medicineDetails',
            name: 'medicineDetails',
            meta: { title: '资讯详情' },
            component: () =>
                import ('@/views/information/medicine_details')

        }, ]
    },
    //评论
    {
        path: '/commentsPage',
        name: 'commentsPage',
        component: comheader,
        // meta: { title: '评论列表' },
        redirect: '/commentsPage/comments',
        children: [{
            path: 'comments',
            name: 'comments',
            meta: { title: '全部评论' },
            component: () =>
                import ('@/views/personal/comments')
        }, {
            path: 'commentReply',
            name: 'commentReply',
            meta: { title: '评论回复', fatherName: 'comments' },
            component: () =>
                import ('@/views/personal/commentReply')
        }]
    },
    //评论
    {
        path: '/componentsCom',
        name: 'componentsCom',
        component: specialHeader,
        children: [{
            path: '/aboutUs',
            name: 'aboutUs',
            meta: { title: '关于我们' },
            component: () =>
                import ('@/views/home/aboutUs')
        }, {
            path: '/review',
            name: 'review',
            meta: { title: '发表评价', fatherName: 'order' },
            component: () =>
                import ('@/views/personal/review')
        }, {
            path: '/pay',
            name: 'pay',
            meta: { title: '支付订单' },
            component: () =>
                import ('@/views/personal/pay')
        }]
    },

    // 消息
    {
        path: '/componentsCom',
        name: 'componentsCom',
        component: specialHeader,
        redirect: '/componentsCom/message',
        children: [{
            path: 'message',
            name: 'message',
            meta: { title: '消息' },
            component: () =>
                import ('@/views/home/message')
        }, {
            path: 'messageDetails',
            name: 'messageDetails',
            meta: { title: '消息详情', fatherName: 'message' },
            component: () =>
                import ('@/views/home/message-details')
        }]
    },
    // 购物车
    {
        path: '/shopping',
        name: 'shopping',
        component: specialHeader,
        children: [{
                path: '/shoppingCart',
                name: 'shoppingCart',
                meta: { title: '购物车' },
                component: () =>
                    import ('@/views/shopping/index')
            },
            {
                path: '/confirmOrder',
                name: 'confirmOrder',
                meta: { title: '确认订单' },
                component: () =>
                    import ('@/views/shopping/confirm-order')
            },
            {
                path: '/orderDetails',
                name: 'orderDetails',
                meta: { title: '订单详情', fatherName: 'order' },
                component: () =>
                    import ('@/views/personal/order-details')
            },
            {
                path: '/review',
                name: 'review',
                meta: { title: '商品评价' },
                component: () =>
                    import ('@/views/personal/review')
            },
            {
                path: '/wechat',
                name: 'wechat',
                title: '微信支付',
                component: () =>
                    import ('@/views/personal/wechat-pay')
            },
            {
                path: '/recharge',
                name: 'recharge',
                meta: { title: '账户余额' },
                component: () =>
                    import ('@/views/personal/recharge')
            },
            {
                path: '/success',
                name: 'success',
                component: () =>
                    import ('@/views/personal/pay-success')
            }
        ]
    },
    {
        path: '/personal',
        name: 'personal',
        meta: { title: '个人中心' },
        component: perHeader,
        children: [{
                path: '/personalIndex',
                name: 'personalIndex',
                meta: { title: '个人中心' },
                component: () =>
                    import ('@/views/personal/index')
            }, {
                path: '/invoice',
                name: 'invoice',
                meta: { title: '我的发票' },
                component: () =>
                    import ('@/views/personal/invoice')

            }, {
                path: '/editPerson',
                name: 'editPerson',
                meta: { title: '修改个人信息' },
                component: () =>
                    import ('@/views/personal/edit-person')
            },
            {
                path: '/invoiceDetails',
                name: 'invoiceDetails',
                meta: { title: '发票详情' },
                component: () =>
                    import ('@/views/personal/invoice-details')
            },
            {
                path: '/bookshelf',
                name: 'bookshelf',
                meta: { title: '我的书架' },
                component: () =>
                    import ('@/views/personal/my-bookshelf')
            },
            {
                path: '/favorite',
                name: 'favorite',
                meta: { title: '我的收藏' },
                component: () =>
                    import ('@/views/personal/favorite')
            },
            {
                path: '/order',
                name: 'order',
                meta: { title: '我的订单' },
                component: () =>
                    import ('@/views/personal/my-order')
            },
            {
                path: '/interest',
                name: 'interest',
                component: () =>
                    import ('@/views/personal/interest')
            },
            {
                path: '/note',
                name: 'note',
                meta: { title: '我的笔记' },
                component: () =>
                    import ('@/views/personal/my-note')
            },
            {
                path: '/contribute',
                name: 'contribute',
                meta: { title: '我的贡献' },
                component: () =>
                    import ('@/views/personal/my-contribute')
            },
            {
                path: '/browsing',
                name: 'browsing',
                meta: { title: '浏览记录' },
                component: () =>
                    import ('@/views/personal/browsing')
            },
            {
                path: '/set',
                name: 'set',
                meta: { title: '账户安全' },
                component: () =>
                    import ('@/views/personal/set')
            },
            {
                path: '/question',
                name: 'question',
                component: () =>
                    import ('@/views/personal/question')
            }, {
                path: '/comment',
                name: 'comment',
                meta: { title: '我的评论' },
                component: () =>
                    import ('@/views/personal/my-comment')
            }, {
                path: '/account',
                name: 'account',
                component: () =>
                    import ('@/views/personal/account')
            }, {
                path: '/organizationIndex',
                name: 'organizationIndex',
                meta: { title: '机构中心' },
                component: () =>
                    import ('@/views/personal/organizationIndex')
            }
        ]
    },
    {
        path: '/ebook',
        name: 'ebook',
        meta: { title: '阅读器' },
        component: () =>
            import ('@/views/Ebook/ebook')
    },

    {
        path: '/loginIndex',
        name: 'loginIndex',
        meta: { title: '登录' },
        component: LoginHeader,
        children: [{
                path: '/login',
                name: 'login',
                component: () =>
                    import ('@/views/login/index')
            },
            {
                path: '/register',
                name: 'register',
                component: () =>
                    import ('@/views/login/register')
            },
            {
                path: '/number',
                name: 'number',
                component: () =>
                    import ('@/views/login/account-number')
            },
            {
                path: '/create',
                name: 'create',
                component: () =>
                    import ('@/views/login/account-create')
            },
            {
                path: '/password',
                name: 'password',
                component: () =>
                    import ('@/views/login/reset-password')
            }
        ]
    },
]

export default new Router({
    routes: constantRouterMap
})