export const requestPath = {
    member: '/member/fg',
    common: '/sysuser/fg',
    book: '/book/fg',
    content: 'content/fg',
    stock: '/repertory/fg',
    marketing: '/marketing/fg',
    order: '/order/fg',
    resource: '/resource/fg',
    third: '/third',
    file: '/file/file/',
    works: '/works/fg',
    ftp: '/file/ftp',
    pay: '/pay/fg'
}

// export const uploadUrl = `http://10.2.251.251:80/file/`
export const uploadUrl = `http://test16.zhongdianyun.com:8008/file/`
// export const uploadUrl = `http://localhost:8008/file/`

export const downloadPath = 'download'
export const uploadPath = 'upload'