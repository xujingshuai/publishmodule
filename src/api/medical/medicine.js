import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

//列表
export function getNameAPI(params) {
    return request({
        url: `${requestPath.common}/bulletin-column/list`,
        method: 'get',
        params
    })
}

//列表
export function getListAPI(params) {
    return request({
        url: `${requestPath.common}/bulletin-content/page`,
        method: 'get',
        params
    })
}


//详情
export function medicalDetailsAPI(id) {
    return request({
        url: `${requestPath.common}/bulletin-content/${id}`,
        method: 'get'
    })
}