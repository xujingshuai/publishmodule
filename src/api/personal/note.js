import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getNoteListAPI(params) { //我的笔记列表
    return request({
        url: `${requestPath.resource}/note/list`,
        method: 'get',
        params
    })
}

export function delNoteAPI(data) { //删除
    return request({
        url: `${requestPath.resource}/note/batch/delete`,
        method: 'post',
        data
    })
}