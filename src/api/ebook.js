import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getcatalogueAPI(params) { //目录
    return request({
        url: `${requestPath.resource}/article-library/directory`,
        method: 'get',
        params
    })
}

export function saveCollectionAPI(data) { //点击收藏
    return request({
        url: `${requestPath.resource}/member-collection/batch/save`,
        method: 'post',
        data
    })
}
export function cancelcollectionAPI(data) { //取消收藏
    return request({
        url: `${requestPath.resource}/member-collection/batch/delete`,
        method: 'post',
        data
    })
}

export function isCollectionAPI(params) { //是否收藏
    return request({
        url: `${requestPath.resource}/member-collection/check-is-collect`,
        method: 'get',
        params
    })
}

