import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(params) { //购物车-列表
    return request({
        url: `${requestPath.resource}/shopping-cart/list`,
        method: 'get',
        params
    })
}

export function deleteAPI(data) { //批量删除
    return request({
        url: `${requestPath.resource}/shopping-cart/batch/delete`,
        method: 'post',
        data
    })
}

export function recommendAPI(params) { //推荐列表
    return request({
        url: `${requestPath.resource}/ebook/recommend`,
        method: 'get',
        params
    })
}

export function favoriteAPI(data) { //收藏
    return request({
        url: `${requestPath.resource}/member-collection/batch/save`,
        method: 'post',
        data
    })
}

export function shoppingAPI(params) { //确认订单列表
    return request({
        url: `${requestPath.resource}/shopping-cart/batch/get`,
        method: 'get',
        params
    })
}

export function createOrderAPI(data) { //生成订单
    return request({
        url: `${requestPath.order}/order/create-order`,
        method: 'post',
        data
    })
}

export function getGoodsInfo(params) { //确认订单列表
    return request({
        url: `${requestPath.resource}/resource/get/info`,
        method: 'get',
        params
    })
}


export function favoriteMoveAPI(data) { //移入收藏
    return request({
        url: `${requestPath.resource}/member-collection/shopping-cart/batch/save`,
        method: 'post',
        data
    })
}

export function isRepeatAPI(params) { //是否有重复资源，同一本书
    return request({
        url: `${requestPath.resource}/shopping-cart/batch/get/isExist`,
        method: 'get',
        params
    })
}

//确认收货地址
export function confirmAddressAPI(data) { //移入收藏
    return request({
        url: `${requestPath.resource}/shopping-address/save`,
        method: 'post',
        data
    })
}

//确认收货地址
export function getAddressAPI() { //获取默认地址
    return request({
        url: `${requestPath.resource}/shopping-address/get/default`,
        method: 'get',
    })
}

//购物车商品数量发生变化
export function changeNumAPI(data) { //移入收藏
    return request({
        url: `${requestPath.resource}/shopping-cart/update/count`,
        method: 'post',
        data
    })
}

//结算时是否有实体图书0无1有
export function hasBookAPI(params) { //是否有重复资源，同一本书
    return request({
        url: `${requestPath.resource}/shopping-cart/entity/isExist`,
        method: 'get',
        params
    })
}