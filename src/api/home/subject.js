import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function subjectListAPI(params) { //获取列表
    return request({
        url: `${requestPath.resource}/resource/search`,
        method: 'get',
        params
    })
}

export function getSubjectDetailsAPI(id) { //获取专题详情
    return request({
        url: `${requestPath.resource}/project-library/get/${id}`,
        method: 'get'
    })
}

export function getSubjectBookListAPI(params) { //获取相关图书
    return request({
        url: `${requestPath.resource}/project-library/relation-resource/book`,
        method: 'get',
        params
    })
}


export function getSubjectResourceAPI(params) { //获取相关资源
    return request({
        url: `${requestPath.resource}/project-library/relation-resource/list`,
        method: 'get',
        params
    })
}
