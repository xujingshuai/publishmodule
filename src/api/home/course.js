import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function recommendAPI(params) { //课程推荐
    return request({
        url: `${requestPath.resource}/recommend/content/list`,
        method: 'get',
        params
    })
}

export function recommendImgAPI(params) { //广告位
    return request({
        url: `${requestPath.common}/adspace/signvalue`,
        method: 'get',
        params
    })
}

export function courseListAPI(params) { //课程列表
    return request({
        url: `${requestPath.resource}/resource/search`,
        method: 'get',
        params
    })
}

export function courseDetailsAPI(id) { //课程详情
    return request({
        url: `${requestPath.resource}/course-library/get/${id}`,
        method: 'get'
    })
}

export function courseCommonAPI(params) { //课程目录/配套资源/配套图书
    return request({
        url: `${requestPath.resource}/resource/relation-resource/page`,
        method: 'get',
        params
    })
}

export function courseResourceAPI(params) { //相关资源
    return request({
        url: `${requestPath.resource}/resource/relation-resource`,
        method: 'get',
        params
    })
}

export function courseAdspaceAPI(params) { //课程推荐页--第一个推荐资源
    return request({
        url: `${requestPath.common}/adspace/signvalue`,
        method: 'get',
        params
    })
}