import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getListAPI(params) { //列表 //资源类型   1图书、2医家、3文章、4图片、5视频、6音频、7课件 
    return request({
        url: `${requestPath.resource}/resource/search`,
        method: 'get',
        params
    })
}

//检索历史
export function getHistoryAPI(params) {
    return request({
        url: `${requestPath.resource}/search/record/list`,
        method: 'get',
        params
    })
}


//自定义分类
export function resourceDataAPI(params) {
    return request({
        url: `${requestPath.resource}/diytype/tree`,
        method: 'get',
        params
    })
}