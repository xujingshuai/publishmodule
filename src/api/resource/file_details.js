import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function fileDetailAPI(params) { //课件详情页
    return request({
        url: `${requestPath.resource}/annex-library/get/${params}`,
        method: 'get',
    })
}