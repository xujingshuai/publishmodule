import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function getImageAPI(params) { //图片
    return request({
        url: `${requestPath.common}/adspace/signvalue`,
        method: 'get',
        params
    })
}

export function getSortAPI(params) { //获取一级分类名字
    return request({
        url: `${requestPath.resource}/diytype/list`,
        method: 'get',
        params
    })
}

export function getRecommondAPI(params) { //获取一级分类名字
    return request({
        url: `${requestPath.resource}/recommend/content/list`,
        method: 'get',
        params
    })
}