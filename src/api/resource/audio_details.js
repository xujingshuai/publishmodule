import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function audioDetailAPI(params) { //视频详情页
    return request({
        url: `${requestPath.resource}/audio-library/get/${params}`,
        method: 'get',
    })
}