import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function videoDetailAPI(params) { //视频详情页
    return request({
        url: `${requestPath.resource}/video-library/get/${params}`,
        method: 'get',
    })
}