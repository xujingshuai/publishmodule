import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function imageDetailAPI(params) { //图片详情页
    return request({
        url: `${requestPath.resource}/image-library/${params}`,
        method: 'get',
    })
}

export function getCommentListAPI(params) { //获取评论列表
    return request({
        url: `${requestPath.common}/comment/page`,
        method: 'get',
        params
    })
}

export function commentSaveAPI(data) { //评论或回复
    return request({
        url: `${requestPath.common}/comment/save`,
        method: 'post',
        data
    })
}

export function collectionAPI(data) { //点击收藏
    return request({
        url: `${requestPath.resource}/member-collection/batch/save`,
        method: 'post',
        data
    })
}

export function cancelcollectionAPI(data) { //取消收藏
    return request({
        url: `${requestPath.resource}/member-collection/batch/delete`,
        method: 'post',
        data
    })
}

export function commentAgreeAPI(data) { //点赞
    return request({
        url: `${requestPath.common}/comment/agree?commentId=${data.commentId}`,
        method: 'post',
        data
    })
}

export function shopCartAPI(data) { //添加购物车
    return request({
        url: `${requestPath.resource}/shopping-cart/save`,
        method: 'post',
        data
    })
}