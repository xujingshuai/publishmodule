/**
 * @Author:      skyeGao
 * @Email:       yyjzp1314@126.com
 * @DateTime:    2019-04-11 20:34:54
 * @Description: 资源检索 相关API 
 */

import request from '@/utils/request'

// 查询条件和数据 在一个接口
/*
diyTypeCode: '',              // 自定义分类code 多个用,分隔
labelContentDiyTypeId: '',    // 标签内容id  多个用,分隔
content: '',                  // 上面搜索框内容
resourceType: '',             // 资源类型 0全部1图书 31文章 23案例 33实操技能 34法规 35问答 2微课
pageNum: '',                  //
pageSize: '',                 //
orderBy: ''                   // 
*/

// 文章阅读页 - 获取电子书内容
export const getEbookContent = (params) => {
  return request({
    url: '/resource/fg/article-library/get',
    method: 'get',
    params:{...params},
  })
}

// 文章阅读页 - 电子书目录
export const getEbookCatalogue = (params) => {
  return request({
    url: '/resource/fg/article-library/directory',
    method: 'get',
    params:{...params},
  })
}

export const saveCorrect = (data) => { //保存纠错
  return request({
      url: `sysuser/fg/error-correct/save`,
      method: 'post',
      data,
  })
}


// 文章阅读页 - 新增笔记
export const articalNoteAdd = (data) => {
 /*const params = {
    articleId: 文章id ,
    bookId: 图书id ,
    content: 笔记内容 ,
    contentTitle: 笔记标题 ,
    createTime: 创建时间 ,
    creater: 用户id ,
    dataContent: 添加笔记的段落内容 ,
    dataMark: 段落标识（文章的markid#_#段落编号） ,
    id: 笔记id ,
    markId: markId
  }*/
  return request({
    url: '/resource/fg/note/add',
    method: 'post',
    data,
  })
}
// 文章阅读页 - 修改笔记
export const articalNoteUpdate = (data) => {
  /*const params = {
     articleId: 文章id ,
     bookId: 图书id ,
     content: 笔记内容 ,
     contentTitle: 笔记标题 ,
     createTime: 创建时间 ,
     creater: 用户id ,
     dataContent: 添加笔记的段落内容 ,
     dataMark: 段落标识（文章的markid#_#段落编号） ,
     id: 笔记id ,
     markId: markId
   }*/
   return request({
     url: '/resource/fg/note/update',
     method: 'post',
     data,
   })
 }
 // 文章阅读页 - 删除笔记
export const articalNoteDel = (data) => {
   return request({
     url: '/resource/fg/note/batch/delete',
     method: 'post',
     data,
   })
 }
// 文章阅读页 - 笔记列表获取
export const articalNoteList = (params) => {
  return request({
    url: '/resource/fg/note/list-by-markid',
    method: 'get',
    params: { ...params }
  })
}

// 电子书阅读页 - 左侧关联资源
export const getRelationResource = (params) => { 
  return request({
      url: `/resource/fg/resource/relation-resource`,
      method: 'get',
      params: { ...params }
  })
}





