import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function phoneAPI(params) { //发送手机短信
    return request({
        url: `${requestPath.member}/permissions/password/sendmessage`,
        method: 'get',
        params
    })
}

export function checkPhoneAPI(params) { //验证手机短信
    return request({
        url: `${requestPath.member}/permissions/password/verifymessage`,
        method: 'get',
        params
    })
}

export function passwordAPI(data) { //新密码
    return request({
        url: `${requestPath.member}/permissions/password/inputpassword`,
        method: 'post',
        data
    })
}