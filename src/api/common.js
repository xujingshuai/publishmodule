import request from '@/utils/request'
import { requestPath } from '@/utils/global.js'

export function linksAPI(params) { //友情链接
    return request({
        url: `${requestPath.common}/home-page/links/list`,
        method: 'get',
        params
    })
}

export function footerAPI(params) { //页脚
    return request({
        url: `${requestPath.common}/footer/page`,
        method: 'get',
        params
    })
}

export function footerDetailsAPI(params) { //页脚详情
    return request({
        url: `${requestPath.common}/footer/${params}`,
        method: 'get',
    })
}

export function dictionaryAPI(params) { //获取底部版权
    return request({
        url: `${requestPath.common}/dictionary/get`,
        method: 'get',
        params
    })
}

export function shopCartAPI(data) { //添加购物车
    return request({
        url: `${requestPath.resource}/shopping-cart/save`,
        method: 'post',
        data
    })
}

export function messageAPI(params) { //查询是否有未读消息
    return request({
        url: `${requestPath.member}/pm/count`,
        method: 'get',
        params
    })
}


export function getCommentListAPI(params) { //获取评论列表
    return request({
        url: `${requestPath.common}/comment/page`,
        method: 'get',
        params
    })
}

export function commentSaveAPI(data) { //评论或回复
    return request({
        url: `${requestPath.common}/comment/save`,
        method: 'post',
        data
    })
}

export function collectionAPI(data) { //点击收藏
    return request({
        url: `${requestPath.resource}/member-collection/batch/save`,
        method: 'post',
        data
    })
}

export function cancelcollectionAPI(data) { //取消收藏
    return request({
        url: `${requestPath.resource}/member-collection/batch/delete`,
        method: 'post',
        data
    })
}

export function commentAgreeAPI(data) { //点赞
    return request({
        url: `${requestPath.common}/comment/agree?commentId=${data.commentId}`,
        method: 'post',
        data
    })
}

export function searchTipAPI(params) { //模糊搜索提示
    return request({
        url: `${requestPath.resource}/resource/search/list`,
        method: 'get',
        params
    })
}

export function bookSumRankingAPI(params) { //图书总排行榜
    return request({
        url: `${requestPath.resource}/ebook/other/list`,
        method: 'get',
        params
    })
}

export function commonRecommendAPI(params) { //章节、图片、视频、音频、课件、在线课程、专题详情页的相关资源接口
    return request({
        url: `${requestPath.resource}/resource/relation-resource`,
        method: 'get',
        params
    })
}

export function getIndexImgAPI(params) { //首页封面
    return request({
        url: `${requestPath.resource}/recommend/content/list`,
        method: 'get',
        params
    })
}



export function hasCourseAPI() { //判断时候有课程
    return request({
        url: `${requestPath.resource}/course-library/isExist`,
        method: 'get',
    })
}