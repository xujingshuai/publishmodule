'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
    NODE_ENV: '"testing"',
    // BASE_API: '"http://10.2.251.251:80"'
    BASE_API: '"http://test16.zhongdianyun.com:8008"'
    // BASE_API: '"http://localhost:8008"',
})