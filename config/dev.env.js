'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    ENV_CONFIG: '"dev"',
    // BASE_API: '"http://10.2.251.251:80"'
    BASE_API: '"http://test16.zhongdianyun.com:8008"'
    // BASE_API: '"http://192.168.2.173:8008"'
})